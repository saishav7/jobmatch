package com.jobmatch.service;

import com.jobmatch.model.Job;
import retrofit2.Call;
import retrofit2.http.GET;

import java.util.List;


/**
 * Created by saishav7
 */

public interface JobsDataService {

    @GET("jobs")
    Call<List<Job>> listJobs();

}
