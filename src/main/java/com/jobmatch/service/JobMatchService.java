package com.jobmatch.service;

import com.jobmatch.model.Job;
import com.jobmatch.model.Worker;
import com.jobmatch.util.RetrofitUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import retrofit2.Retrofit;

import java.io.IOException;
import java.util.*;

/**
 * Created by saishav7
 */

@Service
public class JobMatchService {

    private static final Logger log = LoggerFactory.getLogger(JobMatchService.class);

    public static final String BASE_URL = "http://test.swipejobs.com/api/";
    public static final int MAX_JOBS = 3;



    /**
     * @param workerId
     * @return Returns a list of up to 3 matched jobs for the given worker. Matching assumes equal weightage for all
     * properties. It is based on the following criteria
     * Worker status active/inactive
     * Worker available dates
     * Worker location
     * Worker certificates
     * Worker possession of driver's license or not
     */
    public List<Job> getMatchedJobsForWorkerId(int workerId) {
        List<Job> jobs = getJobs();
        List<Worker> workers = getWorkers();
        List<Job> matchedJobs = new ArrayList<>();

        if (CollectionUtils.isEmpty(jobs) || CollectionUtils.isEmpty(workers)) {
            log.error("Cannot perform matching due to missing data");
            return matchedJobs;
        }

        Worker matchedWorker = workers.stream()
                .filter(worker -> worker.getUserId() == workerId)
                .findFirst()
                .orElse(null);

        if (matchedWorker != null) {
            matchedJobs = getMatchedJobsForWorker(matchedWorker, jobs);
        } else {
            log.error("No worker found with id {}", workerId);
        }
        return matchedJobs;
    }

    private List<Job> getMatchedJobsForWorker(Worker worker, List<Job> jobs) {

        List<Job> matchedJobs = new ArrayList<>();

        if (!worker.getIsActive()) {
            return matchedJobs;
        }

        for(Job job : jobs) {
            boolean matches = Optional.of(new JobMatchEvaluator(worker, job))
                    .map(JobMatchEvaluator::matchesLocation)
                    .map(JobMatchEvaluator::matchesAvailability)
                    .map(JobMatchEvaluator::matchesCertificateRequirement)
                    .map(JobMatchEvaluator::matchesDriverLicenseRequirement)
                    .isPresent();

            if (matches) {
                if (matchedJobs.size() < MAX_JOBS) {
                    matchedJobs.add(job);
                } else {
                    return matchedJobs;
                }
            }
        }
        return matchedJobs;
    }


    private List<Job> getJobs() {
        try {
            Retrofit retrofit = RetrofitUtil.buildHttpRequest(BASE_URL);
            JobsDataService jobsDataService = retrofit.create(JobsDataService.class);
            return jobsDataService.listJobs().execute().body();
        } catch (IOException e) {
            log.error("Exception occurred while calling the API to get jobs", e);
            return null;
        }
    }

    private List<Worker> getWorkers() {
        try {
            Retrofit retrofit = RetrofitUtil.buildHttpRequest(BASE_URL);
            WorkersDataService workersDataService = retrofit.create(WorkersDataService.class);
            return workersDataService.listWorkers().execute().body();
        } catch (IOException e) {
            log.error("Exception occurred while calling the API to get workers", e);
            return null;
        }
    }
}
