package com.jobmatch.service;

import com.jobmatch.model.Worker;
import retrofit2.Call;
import retrofit2.http.GET;

import java.util.List;

/**
 * Created by saishav7
 */
public interface WorkersDataService {

    @GET("workers")
    Call<List<Worker>> listWorkers();
}
