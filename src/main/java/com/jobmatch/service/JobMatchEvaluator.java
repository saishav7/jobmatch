package com.jobmatch.service;

import com.javadocmd.simplelatlng.LatLng;
import com.javadocmd.simplelatlng.LatLngTool;
import com.javadocmd.simplelatlng.util.LengthUnit;
import com.jobmatch.model.Job;
import com.jobmatch.model.Worker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;

/**
 * Created by saishav7
 */
public class JobMatchEvaluator {

    private static final Logger log = LoggerFactory.getLogger(JobMatchEvaluator.class);

    private Job job;

    private Worker worker;

    public JobMatchEvaluator(Worker worker, Job job) {
        this.worker = worker;
        this.job = job;
    }

    public JobMatchEvaluator matchesLocation() {
        LatLng jobLocation = new LatLng(job.getLocation().getLatitude(), job.getLocation().getLongitude());
        LatLng workerLocation
                = new LatLng(worker.getJobSearchAddress().getLatitude(), worker.getJobSearchAddress().getLongitude());
        if (LatLngTool.distance(workerLocation, jobLocation, LengthUnit.KILOMETER)
                <= worker.getJobSearchAddress().getMaxJobDistance()) {
            return this;
        }
        return null;
    }

    public JobMatchEvaluator matchesDriverLicenseRequirement() {
        if (!job.isDriverLicenseRequired() || worker.isHasDriversLicense()) {
            return this;
        }
        return null;
    }

    public JobMatchEvaluator matchesCertificateRequirement() {
        int flag = 0;
        for (String certificate : job.getRequiredCertificates()) {
            if (!worker.getCertificates().contains(certificate)) {
                flag = 1;
            }
        }
        if (flag == 0) {
            return this;
        }
        return null;
    }

    public JobMatchEvaluator matchesAvailability() {

        LocalDateTime jobStartDateTime =
                Optional.ofNullable(job.getStartDate())
                        .map(Date::toInstant)
                        .map(i -> i.atZone(ZoneId.systemDefault()))
                        .map(ZonedDateTime::toLocalDateTime)
                        .orElse(null);

        if (worker.getAvailability().stream()
                .anyMatch(availability -> availability.getDayIndex() == jobStartDateTime.getDayOfWeek().getValue())) {
            return this;
        }
        return null;
    }

}
