package com.jobmatch.model;

import java.util.Map;

/**
 * Created by saishav7
 */
public class Error {

    public Integer status;
    public String error;
    public String message;
    public String timestamp;
    public String trace;

    public Error(int status, Map<String, Object> errorAttributes) {
        this.status = status;
        this.error = (String) errorAttributes.get("error");
        this.message = (String) errorAttributes.get("message");
        this.timestamp = errorAttributes.get("timestamp").toString();
        this.trace = (String) errorAttributes.get("trace");
    }

}