package com.jobmatch.model;

import java.util.Date;
import java.util.Set;

/**
 * Created by saishav7
 */
public class Job {

    private long jobId;
    private String guid;
    private Set<String> requiredCertificates;
    private boolean driverLicenseRequired;
    private Location location;
    private int workersRequired;
    private Date startDate;
    private String billRate;
    private String about;
    private String jobTitle;
    private String company;

    public long getJobId() {
        return jobId;
    }

    public void setJobId(long jobId) {
        this.jobId = jobId;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public Set<String> getRequiredCertificates() {
        return requiredCertificates;
    }

    public void setRequiredCertificates(Set<String> requiredCertificates) {
        this.requiredCertificates = requiredCertificates;
    }

    public boolean isDriverLicenseRequired() {
        return driverLicenseRequired;
    }

    public void setDriverLicenseRequired(boolean drivingLicenseRequired) {
        this.driverLicenseRequired = drivingLicenseRequired;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getWorkersRequired() {
        return workersRequired;
    }

    public void setWorkersRequired(int workersRequired) {
        this.workersRequired = workersRequired;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getBillRate() {
        return billRate;
    }

    public void setBillRate(String billRate) {
        this.billRate = billRate;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
