package com.jobmatch.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by saishav7
 */
public class RetrofitUtil {

    private static final Logger log = LoggerFactory.getLogger(RetrofitUtil.class);

    public static Retrofit buildHttpRequest(String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
    }
}
