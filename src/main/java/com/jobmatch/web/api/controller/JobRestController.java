package com.jobmatch.web.api.controller;

import com.jobmatch.model.Job;
import com.jobmatch.service.JobMatchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by saishav7
 */

@RestController
public class JobRestController {

    @Autowired
    private JobMatchService jobMatchingService;

    private static final Logger log = LoggerFactory.getLogger(JobRestController.class);

    @GetMapping("/api/matches/{workerId}")
    public List<Job> getJobs(@PathVariable("workerId") int workerId) {
        return jobMatchingService.getMatchedJobsForWorkerId(workerId);
    }

}
